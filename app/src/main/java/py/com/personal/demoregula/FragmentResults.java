package py.com.personal.demoregula;

import android.graphics.Bitmap;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.regula.documentreader.api.enums.eGraphicFieldType;
import com.regula.documentreader.api.enums.eVisualFieldType;
import com.regula.documentreader.api.results.DocumentReaderResults;
import com.regula.documentreader.api.results.DocumentReaderTextField;
import com.regula.facesdk.results.MatchFacesResponse;
import com.regula.facesdk.results.MatchedFacesPair;
import com.regula.facesdk.structs.Image;
import com.regula.facesdk.structs.MatchFacesRequest;
import java.util.ArrayList;



public class FragmentResults extends Fragment {
    private TextView comparisonTv;
    private EditText edtxtNroDoc;
    private EditText edtxtFechaNac;
    private EditText edtxtNombres;
    private EditText edtxtApellidos;
    private RadioGroup radioGroupSexo;
    private RadioGroup radioGroupComparacion;
    private ImageView docImageIv;
    private ImageView realIv;
    private LinearLayout authResults;

    private DocumentReaderResults results;
    private MatchFacesRequest request;
    private MatchFacesResponse response;

    private static FragmentResults instance;

    static FragmentResults getInstance(DocumentReaderResults results){
        return getInstance(results, null, null);
    }

    static FragmentResults getInstance(DocumentReaderResults results, MatchFacesRequest request, MatchFacesResponse response){
        if(instance==null){
            instance = new FragmentResults();
        }
        instance.results = results;
        instance.request = request;
        instance.response = response;

        return instance;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_results,container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        edtxtNroDoc = view.findViewById(R.id.txtNroDoc);
        edtxtFechaNac = view.findViewById(R.id.txtFechaNac);
        radioGroupSexo = view.findViewById(R.id.radiogroup_sexo);
        radioGroupComparacion = view.findViewById(R.id.radiogroup_comparacion);
        edtxtNombres = view.findViewById(R.id.txtNombres);
        edtxtApellidos = view.findViewById(R.id.txtApellidos);
        comparisonTv = view.findViewById(R.id.comparisonValue);

        docImageIv = view.findViewById(R.id.documentImageIv);
        realIv = view.findViewById(R.id.realImage);

        authResults = view.findViewById(R.id.authHolder);

        displayResults(this.results);
    }

    private void clearResults(){
        edtxtNroDoc.setText("");
        edtxtFechaNac.setText("");
        radioGroupSexo.setActivated(false);
        edtxtNombres.setText("");
        edtxtApellidos.setText("");
        docImageIv.setImageResource(R.drawable.id);
    }


    //show received results on the UI
    private void displayResults(DocumentReaderResults results){
        clearResults();

        if(results!=null) {
            setCamposCedula();

            // through all text fields
            if(results.textResult != null && results.textResult.fields != null) {
                for (DocumentReaderTextField textField : results.textResult.fields) {
                    String value = results.getTextFieldValueByType(textField.fieldType, textField.lcid);
                    Log.d("MainActivity", value + "\n");
                }
            }

            if(response!=null && response.matchedFaces!=null && response.matchedFaces.size()>0){
                MatchedFacesPair pair = response.matchedFaces.get(0);
                comparisonTv.setText("Value: " + (int)(pair.similarity * 100) + "%" );

                for(Image img : request.images){
                    if(img.id == MainActivity.CAPTURED_FACE){
                        realIv.setImageBitmap(img.image());
                    }
                }

            } else {
                authResults.setVisibility(View.INVISIBLE);
            }


            Bitmap documentImage = results.getGraphicFieldImageByType(eGraphicFieldType.GF_DOCUMENT_IMAGE);
            if(documentImage!=null){
                double aspectRatio = (double) documentImage.getWidth() / (double) documentImage.getHeight();
                documentImage = Bitmap.createScaledBitmap(documentImage, (int)(480 * aspectRatio), 480, false);
                docImageIv.setImageBitmap(documentImage);
            }
        }
    }

    private void setCamposCedula() {
        MatchedFacesPair pair = response.matchedFaces.get(0);
        String txtFieldNombres = results.getTextFieldValueByType(eVisualFieldType.FT_GIVEN_NAMES);
        String txtFieldApellidos = results.getTextFieldValueByType(eVisualFieldType.FT_SURNAME);
        String txtFieldSexo = results.getTextFieldValueByType(eVisualFieldType.FT_SEX);
        String txtFieldFechaNac = results.getTextFieldValueByType(eVisualFieldType.FT_DATE_OF_BIRTH);

        String txtFieldNroDoc = results.getTextFieldValueByType(eVisualFieldType.FT_DOCUMENT_NUMBER);
        ArrayList<String> camposError = new ArrayList<>();

        if (txtFieldNroDoc != null && !txtFieldNroDoc.isEmpty() &&
                txtFieldNroDoc.matches("[0-9]+")) {
            edtxtNroDoc.setText(txtFieldNroDoc);
        } else {
            edtxtNroDoc.setText("");
            camposError.add("número de documento");
        }
        if (txtFieldNombres != null) {
            edtxtNombres.setText(txtFieldNombres);
        } else {
            edtxtNombres.setText("");
            camposError.add("nombre");
        }
        if (txtFieldApellidos != null) {
            edtxtApellidos.setText(txtFieldApellidos);
        } else {
            edtxtApellidos.setText("");
            camposError.add("apellido");
        }

        double valor = pair.similarity *100;

        if(valor >= 80){
            radioGroupComparacion.check(R.id.radio_a);
        }else{
            radioGroupComparacion.check(R.id.radio_r);
        }

        if (txtFieldSexo != null) {
            if (!txtFieldSexo.isEmpty() && txtFieldSexo.matches("Masculino")) {
                radioGroupSexo.check(R.id.radio_m);
            } else if (!txtFieldSexo.isEmpty() && txtFieldSexo.matches("Femenino")) {
                radioGroupSexo.check(R.id.radio_f);
            }
        } else {
            radioGroupSexo.clearCheck();
            camposError.add("sexo");
        }
        if (txtFieldFechaNac != null && !txtFieldFechaNac.isEmpty()){
            edtxtFechaNac.setText(txtFieldFechaNac);
        } else {
            edtxtFechaNac.setText("");
            camposError.add("fecha de nacimiento");
        }

        if (!camposError.isEmpty()) {
            StringBuilder mensajeError = new StringBuilder();
            if (camposError.size() == 1) {
                mensajeError.append("No se pudo obtener el dato del campo ").append(camposError.get(0));
            } else {
                mensajeError.append("No se pudieron obtener los datos de los campos ");
                for (int i = 0; i < camposError.size(); i++) {
                    if (i == camposError.size() - 1)
                        mensajeError.append(camposError.get(i));
                    else
                        mensajeError.append(camposError.get(i)).append(", ");
                }
            }
            Toast.makeText(getContext(),mensajeError.toString(),Toast.LENGTH_SHORT).show();
        }
    }




    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}